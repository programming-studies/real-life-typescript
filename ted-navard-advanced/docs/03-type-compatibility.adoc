== Type Compatibility
:src_path: ..

=== Type Assertion

We can use a *type assertion* to mean “I want to use this as a string”.

[source,typescript,lineos]
----
include::{src_path}/e02.ts[]
----

1. `someValue.valueOf` is available on almost any value.
+
[NOTE]
.null prototype
====
One exception is an object created with `null` as its prototype, which inherits nothing and therefore it nas no `valueOf` property:

----
var obj = Object.create(null);
obj.valueOf();
Uncaught TypeError: obj.valueOf is not a function
----
====

2. This is not an “instance of” test, because there is no inheritance relationship between string and number.

Note that we have to repeat the cast-like assertion to test the condition, and then again inside the if/else blocks.

=== Type Guards

Type Guards getting better and better over time.
Check the link:https://www.typescriptlang.org/docs/handbook/release-notes/typescript-4-4.html[release notes for TypeScript 4.4] for instance.

Besides type assertion (cast-like things), we can also make use of type guards for similar results.

[source,typescript,lineos]
----
include::{src_path}/e03.ts[]
----

1. Creates a type guard function that checks whether the given value is a string.

2. Because we are using a type guard now, we don't need to assert/check the type again inside the if/else/then blocks.

We can also use type guards directly defined inside the body of a function (not defined as a separate type-guard function, like above).

[source,typescript,lineos]
----
include::{src_path}/e04.ts[]
----

We could simplify the `padLeft` function:

[source,typescript,lineos]
----
include::{src_path}/e05.ts[]
----

1. Here the type guard is done using a simple condition.
It is not necessary to always create a type guard function.
Use your best judgement for each case.

2. Because the TypeScript compiler is ridiculously clever, it knows that if the input isn't a number (from the guard in the `if` condition), then it can only be a string and we don't need another type guard here.
