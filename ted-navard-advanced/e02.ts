export const MODULE_NAME = 'E02a';

import { l } from "./utils";

function getNameOrNum(): string | number {
  return (Date.now() % 2) === 0 ? "Twenty Seven" : 27;
}

let norn = getNameOrNum();

//
// Using type assertion. <1>
//
l(norn.valueOf());

// <2>
if ((<string>norn).substring) {
  l((<string>norn).substring(0, 5));
} else if ((<number>norn).toFixed) {
  l((<number>norn).toFixed(2));
}
