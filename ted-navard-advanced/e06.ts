import { l } from "./utils";

type F1 = (x: string, y: string) => void;
type F2 = (x: number, y: number) => void;

const f: F1 & F2 = function f (
  x: string | number,
  y: string | number,
): void {};

// OK <1>
f("foo", "bar");

// OK <2>
f(1e1, 1e2);

// NOK <3>
// f(1, "bar");
// f("foo", 2);
