
const l: Console['log'] = console.log.bind(console);

export {
  l,
};
